<?php
$apiCore = new \App\Api\Core;
$viewer = $apiCore->getViewer();
foreach($terms as $term){
    $term_name = $term->term_title;
}


if (!empty($body)) {
    $body = str_replace('../uploaded/tinymce', url('') . '/uploaded/tinymce', $body);
}
?>

@extends('templates.front_end.master')

@section('content')
    <div id="shopify-section-us_heading" class="shopify-section page_section_heading">
        <div class="page-head tc pr oh page_bg_img page_head_us_heading">
            @include('modals.backdrop')
        </div>
    </div>

    <div class="container mb__50">
        @include('modals.breadcrumb', [
            'text1' => $term_name,
        ])

        <div class="row">
            <div class="col-md-12">
                @if(isset($page_content))
                    @foreach($page_content as $content)
                        <?php echo $content->value ?>
                    @endforeach
                @else
                    <div class="alert alert-danger">
                        Bạn không có quyền xem nội dung này
                    </div>
                @endif
            </div>
        </div>
    </div>

@stop
