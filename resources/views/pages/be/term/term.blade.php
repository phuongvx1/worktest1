@extends('templates.be.master')

@section('content')

    <style type="text/css">
        .bg-ngang-preview2 img {
            width: 100%;
            max-height: 250px;
        }

        .bg_doctor_img img {
            max-width: 500px;
            max-height: 250px;
        }
    </style>

    <?php
    // $pageTitle = (isset($page_title)) ? $page_title : "";
    // $activePage = (isset($active_page)) ? $active_page : "";

    // $apiCore = new \App\Api\Core();

    // $viewer = $apiCore->getViewer();
    ?>

    <div class="row">

       <div class="col-md-12 mb-5">
       @foreach ($terms as $term)
        <li class="c-sidebar-nav-item">
            <input type="text" value=<?php print $term->term_name;?> disabled>
            <input itype="text" value=<?php print($term->term_title)?> style="width:250px" disabled>
            <button class="btn-secondary" onClick="onClick(this)" value=<?php print ($term->term_id);?> name=<?php print ($term->term_name);?> title=<?php print ($term->term_title);?>>
                Edit
            </button>
            @if ($term->term_status == 1)
                <input type="checkbox" onClick="onChange(this)" value=<?php print ($term->term_id);?> checked>
            @else
                <input type="checkbox" onClick="onChange(this)" value=<?php print ($term->term_id);?>>
            @endif
        </li>
        @endforeach
       </div>

       <div class="col-6">
            <h3 class="text-center">Sửa tiêu đề</h3>
            <input class="form-control" type="text" id="update-name"> <br>
            <input class="form-control" type="text" id="update-title"><br>
            <button class="btn-primary" onClick="onUpdate()" id="update-button">Sửa</button>
       </div>

       <div class="col-6">
            <h3 class="text-center">Thêm chính sách</h3>
            <input class="form-control" type="text" id="name"> <br>
            <input class="form-control" type="text" id="title"><br>
            <button class="btn-primary" onClick="onAdd()" id="add-button">Thêm</button>
       </div>
    </div>

    <script type="text/javascript">
        function onClick(e){
            $("#update-name").val(e.name)
            $("#update-title").val(e.title)
            $("#update-button").val(e.value)
        }

        function onAdd(){
            if($("#name").val() == null || $("#title").val() == null || $("#name").val() == "" || $("#title").val() == ""){
                alert("Tiêu đề và đường dẫn không được để trống");
                return;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            type: "POST",
            url: "/admin/terms/add",
            data: {
                term_name: $("#name").val(),
                term_title: $("#title").val(),
            }, success: function () {
                location.reload();
            },
            error: function () {
                alert("Add term fail")
            }
        });
        }

        function onUpdate(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/admin/terms/update",
                data: {
                    term_name: $("#update-name").val(),
                    term_title: $("#update-title").val(),
                    term_id: $("#update-button").val()
                }, success: function () {
                    location.reload();
                },
                error: function () {
                    alert("Update status fail")
                }
            });
        }

        function onChange(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            ck = e.checked == true ? 1 : 0;
            id = e.value;
            $.ajax({
                type: "POST",
                url: "/admin/terms/deactivate",
                data: {
                    term_status: ck,
                    term_id: id
                }, success: function () {
                    alert("Update status successfull");
                },
                error: function () {
                    alert("Update status fail")
                }
            });
        }
    </script>
@stop
