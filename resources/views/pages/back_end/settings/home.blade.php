<?php
$pageTitle = (isset($page_title)) ? $page_title : "";
$activePage = (isset($active_page)) ? $active_page : "";

$apiCore = new \App\Api\Core();
$viewer = $apiCore->getViewer();


?>

@extends('templates.be.master')

@section('content')
    <style type="text/css">
        .cate_preview img,
        .mobi_banner_preview img,
        .banner_preview img {
            width: 100%;
            max-width: 300px;
            height: 100px;
            border: 1px solid;
        }

        .cate_preview img {
            height: 200px;
        }

        .mobi_banner_preview img {
            width: 210px !important;
            height: 200px !important;
        }
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        
        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        
        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        input:checked + .slider {
            background-color: #2196F3;
        }
        
        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }
        
        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }
        
        .slider.round:before {
            border-radius: 50%;
        }

    </style>

    <div>
        <div class="fade-in">
            @if(session('msg'))
                <div class="alert alert-danger">
                    {{session('msg')}}
                </div>
            @endif
            <button class="mb-3 btn-success" data-toggle="modal" data-target="#exampleModal">THÊM BANNER</button>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">THÊM BANNER</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('admin/config/add')}}" method="post" id="frm-add" enctype="multipart/form-data" >
                        <div class="modal-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="title" class="form-control" placeholder="Tiêu đề">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="url" class="form-control" placeholder="Đường dẫn">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Banner (1920 x 621)</label>
                                        <input type="file" name="banner" class="form-control" placeholder="Đường dẫn" require>
                                    </div>
                                </div>              
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobi banner (840 x 800)</label>
                                        <input type="file" name="mobi_banner" class="form-control" placeholder="Đường dẫn" require>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <!-- Button trigger modal -->
            <form action="{{url('admin/config/save')}}" method="post" id="frm-add" enctype="multipart/form-data" >
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body card-block">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="font-weight-bold">4 banners</label>
                                        <div class="row">
                                            <?php $i=1 ?>
                                            @foreach($banners as $banner)
                                            <div class="col-md-12 mb-5">
                                                <div class="index" hidden>{{$i}}</div>
                                                <input type="text" name="banner_id_{{$i}}" value="{{$banner->id}}" hidden>
                                                <div class="row banner_upload banner_upload_{{$i}}">
                                                    <div class="col-md-6 mb-2">
                                                        <label class="text-capitalize">tiêu đề {{$i}}</label>
                                                        <input type="text" name="banner_title_{{$i}}" class="form-control"value="{{$banner->title}}" autocomplete="off"/>
                                                    </div>
                                                    <div class="col-md-6 mb-2">
                                                        <label class="text-capitalize">đường dẫn {{$i}}</label>
                                                        <input type="text" name="banner_link_{{$i}}" class="form-control"value="{{$banner->url}}" autocomplete="off"/>
                                                    </div>
                                                    <div class="col-md-3 mb-2 banner_bg">
                                                        <div class="form-group">
                                                            <label class="text-capitalize">banner {{$i}} (Recommended: 1920 x 621)</label>
                                                            <input type="file" title="{{$i}}" name="img_{{$i}}" id="banner_src_{{$i}}" name="banner_file"  onChange="previewFile(this)" class="form-control" accept="image/*" />
                                                            <div class="alert alert-danger hidden mt-3">Vui lòng không upload hình lớn hơn <b class="max-size-text"></b>.</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 mb-2">
                                                        <div class="form-group banner_upload_preview">
                                                            <div class="banner_preview">
                                                                <img id="img_banner_{{$i}}" src="{{asset('uploaded/sys/' . $banner->banner)}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 mb-2 mobi_banner_bg">
                                                        <div class="form-group">
                                                            <label class="text-capitalize font-weight-bold">mobi banner {{$i}} (Recommended: 840 x 800)</label>
                                                            <input type="file" title="{{$i+1}}" name="img1_{{$i}}" onChange="previewFileMobi(this)" id="mobi_banner_src_{{$i+1}}" class="form-control" accept="image/*" />
                                                            <div class="alert alert-danger hidden mt-3">Vui lòng không upload hình lớn hơn <b class="max-size-text"></b>.</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 mb-2">
                                                        <div class="form-group mobi_banner_upload_preview">
                                                            <div class="mobi_banner_preview">
                                                                <img id="img_mobi_banner_{{$i+1}}" src="{{asset('uploaded/sys/' . $banner->mobi_banner)}}" width="800" height="840"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                    <p>Activate</p>
                                                    @if($banner->status == 1)
                                                    <label class="switch">
                                                        <input
                                                            type="checkbox"
                                                            onClick="onStatusChange(this)"
                                                            title="{{$banner->id}}"
                                                            checked                                                          
                                                        />
                                                        <span class="slider round"></span>
                                                    </div>
                                                    @else
                                                    <label class="switch">
                                                        <input
                                                            type="checkbox" 
                                                            onClick="onStatusChange(this)"
                                                            title="{{$banner->id}}"
                                                        />
                                                        <span class="slider round"></span>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- {{$i++}} -->
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="font-weight-bold">3 giải pháp dưới banner</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php for($i=1;$i<=3;$i++):
                                                $cateBG = $apiCore->getSetting('cate_bg_' . $i);
                                                ?>
                                                <div class="row cate_upload cate_upload_{{$i}}">
                                                    <div class="col-md-3">
                                                        <label class="text-capitalize">tiêu đề {{$i}}</label>
                                                        <input type="text" name="cate_title_{{$i}}" class="form-control"
                                                               value="{{$apiCore->getSetting('cate_title_' . $i)}}" autocomplete="off"
                                                        />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="text-capitalize">đường dẫn {{$i}}</label>
                                                        <input type="text" name="cate_link_{{$i}}" class="form-control"
                                                               value="{{$apiCore->getSetting('cate_link_' . $i)}}" autocomplete="off"
                                                        />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="text-capitalize">hình ảnh {{$i}} (Recommended: 453 x 270)</label>
                                                            <input type="file" name="cate_{{$i}}" class="form-control" accept="image/*" />
                                                            <div class="alert alert-danger hidden mt-3">Vui lòng không upload hình lớn hơn <b class="max-size-text"></b>.</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group cate_upload_preview">
                                                            @if (!empty($cateBG))
                                                                <div class="cate_preview">
                                                                    <img src="{{url('' . $cateBG)}}" />
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endfor;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="font-weight-bold">videos</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="text-capitalize">đường dẫn</label>
                                                <?php for($i=1;$i<=4;$i++):
                                                    ?>
                                                <div class="mb-2">
                                                    <input type="text" name="video_{{$i}}" class="form-control"
                                                           value="{{$apiCore->getSetting('video_' . $i)}}" autocomplete="off"
                                                    />
                                                </div>
                                                <?php endfor;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm mb-1">
                                    <i class="fa fa-check-circle mr-1"></i>
                                    Xác Nhận
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<script>

    function onStatusChange(e){
        let id = e.title;
        let status = e.checked ? 1 : 0;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/admin/config/changeStatus",
            data: {
                id: id,
                status: status
            },success: function(){
                alert('Success')
            }, error: function(){
                alert('Fail')
            }
        
        });
    }
    function previewFile(e) {
        let index = e.title;
        const preview = $('#img_banner_'+index);
        const file = $('#banner_src_' +index).prop('files')[0];
        const reader = new FileReader();

        reader.addEventListener("load", () => {
            preview.prop("src", reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }


    }
    function previewFileMobi(e) {
        let index = e.title;
        const preview = $('#img_mobi_banner_'+index);
        const file = $('#mobi_banner_src_' +index).prop('files')[0];
        const reader = new FileReader();

        reader.addEventListener("load", () => {
            preview.prop("src", reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }
</script>

@stop
