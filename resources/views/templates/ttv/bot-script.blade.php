<?php
$jsVersion = '20220101';
?>

@include('modals.fe')

<script src="{{url('libraries/viewer/jquery.magnify.js')}}" type="text/javascript"></script>

<script src="{{url('libraries/sticky/jquery.sticky.js')}}" type="text/javascript"></script>

<script src="{{url('libraries/uni-carousel/jquery.uni-carousel.js')}}" type="text/javascript"></script>

<script src="{{url('js/main.js?v=' . $jsVersion)}}" type="text/javascript"></script>

<script src="{{url('js/ttv/fe/auth.js?v=' . $jsVersion)}}" type="text/javascript"></script>
<script src="{{url('js/ttv/fe/index.js?v=' . $jsVersion)}}" type="text/javascript"></script>
