<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Dompdf\Dompdf;
use Dompdf\Options;

use \Session;
use App\User;
use App\Api\Core;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Excel\ExportCart;
use App\Excel\ExportCustomer;
use App\Exports\UserExport;
use App\Model\UserCart;
use App\Model\Users;

class ExcelController extends Controller
{
    protected $_apiCore = null;
    protected $_viewer = null;

    public function __construct()
    {
        $this->_apiCore = new Core();
    }

    public function cartExcel($id)
    {
        $cart = UserCart::find((int)$id);
        if (!$cart) {
            return redirect('invalid');
        }

        $fileName = 'don_hang_' . $cart->href . '.xlsx';

        $export = new ExportCart();
        $export->setCart($cart);

        return Excel::download($export, $fileName);
    }

    public function customerExcel()
    {      
        $isAdmin = Session::get('level');
        if($isAdmin !== 1){
            return redirect('invalid');
        }
        return Excel::download(new UserExport(), 'ds_khach_hang_.xlsx');
    }
    public function aaa(){
        return DB::select("Select u.name,u.phone,u.email,uc.paid_date,uc.total_cart
        from users u join user_carts uc on (u.id = uc.user_id) 
        where CONVERT(uc.paid_date, int) in (Select min(CONVERT(paid_date,int)) from user_carts where status = 'da_thanh_toan' group by user_id)");
    }

}
