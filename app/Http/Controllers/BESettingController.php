<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \Session;

use App\Api\Core;

use App\User;
use App\Banner;

use App\Model\Photo;
use App\Model\TermPolicy;

class BESettingController extends Controller
{
    protected $_apiCore = null;
    protected $_viewer = null;

    public function __construct()
    {
        $this->_apiCore = new Core();

        $this->middleware(function ($request, $next) {
            $this->_viewer = $this->_apiCore->getViewer();

            //
            if ($this->_viewer &&
                ($this->_viewer->isDeleted() || $this->_viewer->isBlocked() || !$this->_viewer->isStaff())
            ) {
                return redirect('/invalid');
            }

            return $next($request);
        });

        $this->middleware('auth');
    }

    public function config()
    {
        if (!$this->_viewer->isAllowed('setting_home')) {
            return redirect('/private');
        }

        $saved = (Session::get('SAVED'));
        if ((int)$saved) {
            Session::forget('SAVED');
        }
        $terms = DB::select("Select * from termpolicy");
        $banners = DB::select("Select * from banner");

        $values = [
            'page_title' => 'Tùy Chỉnh Trang Chủ',

            'saved' => $saved,
            'terms' => $terms,
            'banners' => $banners,
        ];

        //message
        $message = (Session::get('MESSAGE'));
        if (!empty($message)) {
            Session::forget('MESSAGE');
        }
        $values['message'] = $message;

        return view("pages.back_end.settings.home", $values);
    }
    public function configAdd(Request $request){

        if(empty($request->file('banner')) || empty($request->file('mobi_banner'))){
            return redirect('admin/config')->with('msg', 'Không thể thiếu banner hoặc mobile banner');
        }
        $banner            = $request->file('banner');
        $banner_extension          = $request->file('banner')->extension();
        $banner_name       = time().'-'.'banner.'.$banner_extension;
        $banner->move(public_path('uploaded/sys'), $banner_name);

        $mobi_banner            = $request->file('mobi_banner');
        $mobi_banner_extension          = $request->file('mobi_banner')->extension();
        $mobi_banner_name       = time().'-'.'mobi-banner.'.$mobi_banner_extension;
        $mobi_banner->move(public_path('uploaded/sys'), $mobi_banner_name);

        DB::insert("Insert into banner(title,url,banner,mobi_banner) values('$request->title','$request->url','$banner_name','$mobi_banner_name')");

        $terms = DB::select("Select * from termpolicy");
        $banners = DB::select("Select * from banner");

        $values = [
            'terms' => $terms,
            'banners' => $banners,
        ];
        return redirect("admin/config")->with('values', $values);

    }

    public function changeStatus(Request $request){
        $id = $request->id;
        $status = $request->status; 
        DB::select("Update banner set status = $status where id = $id");
        $terms = DB::select("Select * from termpolicy");
        $banners = DB::select("Select * from banner");

        $values = [
            'terms' => $terms,
            'banners' => $banners,
        ];
        return redirect("admin/config")->with('values', $values);
    }
    public function configSave(Request $request)
    {
        if (!$this->_viewer->isAllowed('setting_home')) {
            return redirect('/private');
        }

        $values = $request->post();
//        echo '<pre>';var_dump($values, $request->file('site_logo'));die;
        unset($values['_token']);

        $data = DB::select("Select count(id) as count from banner");
        $count = 0;
        foreach($data as $da){
            $count = $da->count;
        }

        for ($i=1;$i<=$count;$i++) {
            $banner_id = $request->input('banner_id_'.$i);
            if (!empty($request->file('img_'.$i))) {
                $banner = $request->file('img_'.$i);
                $banner_extension =$banner->extension();
                $banner_name = time().'-'.'banner.'.$banner_extension;
                $banner->move(public_path('uploaded/sys'), $banner_name);
                DB::update("update banner set banner = '$banner_name' where id = '$banner_id'");
            }

            if (!empty($request->file('img1_'.$i))) {
                $mobi_banner = $request->file('img1_'.$i);
                $mobi_banner_extension = $mobi_banner->extension();
                $mobi_banner_name = time().'-'.'banner.'.$mobi_banner_extension;
                $mobi_banner->move(public_path('uploaded/sys'), $mobi_banner_name);
                DB::update("Update banner set mobi_banner = '$mobi_banner_name' where id = '$banner_id'");
            }
            $title = $request->input('banner_title_'.$i);
            $url = $request->input('banner_link_'.$i);
            DB::update("Update banner set title = '$title', url = '$url' where id = '$banner_id'");

        }

        for ($i=1;$i<=3;$i++) {
            if (!empty($request->file('cate_' . $i))) {
                $imageName = 'home_cate_' . $i . '.' . $request->file('cate_' . $i)->getClientOriginalExtension();
                $imagePath = "/uploaded/sys/" . $imageName;
                $request->file('cate_' . $i)->move(public_path('/uploaded/sys/'), $imageName);

                $values['cate_bg_' . $i] = $imagePath;
            }

            unset($values['cate_' . $i]);
        }

        //save setting home
        if (count($values)) {
            $this->_apiCore->updateSettings($values);
        }

        $this->_apiCore->addLog([
            'user_id' => $this->_viewer->id,
            'action' => 'setting_update',
            'item_id' => 0,
            'item_type' => 'setting',
            'params' => json_encode([
                'type' => 'config_home',
            ])
        ]);

        Session::put('MESSAGE', 'ITEM_UPDATED');

        return redirect('/admin/config');
    }

    public function index()
    {
        if (!$this->_viewer->isAllowed('setting_config')) {
            return redirect('/private');
        }

        $saved = (Session::get('SAVED'));
        if ((int)$saved) {
            Session::forget('SAVED');
        }
        $terms = DB::select("Select * from termpolicy");

        $values = [
            'page_title' => 'Tùy Chỉnh Thông Tin',

            'saved' => $saved,
            'terms' => $terms,

            'logo' => Photo::where('type', 'site_logo')->where('parent_id', 0)->first(),
            'logo2' => Photo::where('type', 'site_logo_ngang')->where('parent_id', 0)->first(),
        ];

        //message
        $message = (Session::get('MESSAGE'));
        if (!empty($message)) {
            Session::forget('MESSAGE');
        }
        $values['message'] = $message;

        return view("pages.back_end.settings.index", $values);
    }

    public function save(Request $request)
    {
        if (!$this->_viewer->isAllowed('setting_config')) {
            return redirect('/private');
        }

        $values = $request->post();
//        echo '<pre>';var_dump($values, $request->file('site_logo'));die;
        unset($values['_token']);

        //logo
        if (!empty($request->file('site_logo'))) {
            $imageName = 'sys_logo.' . $request->file('site_logo')->getClientOriginalExtension();
            $imagePath = "/uploaded/sys/" . $imageName;
            $request->file('site_logo')->move(public_path('/uploaded/sys/'), $imageName);
            $this->_apiCore->uploadLogo($imageName, $imagePath);
        }

        //logo ngang
        if (!empty($request->file('site_logo_ngang'))) {
            $imageName = 'sys_backdrop.' . $request->file('site_logo_ngang')->getClientOriginalExtension();
            $imagePath = "/uploaded/sys/" . $imageName;
            $request->file('site_logo_ngang')->move(public_path('/uploaded/sys/'), $imageName);
            $this->_apiCore->uploadLogo2($imageName, $imagePath);
        }

        unset($values['site_logo']);
        unset($values['site_logo_ngang']);

        //save setting home
        if (count($values)) {
            $this->_apiCore->updateSettings($values);
        }

        $this->_apiCore->addLog([
            'user_id' => $this->_viewer->id,
            'action' => 'setting_update',
            'item_id' => 0,
            'item_type' => 'setting',
            'params' => json_encode([
                'type' => 'config',
            ])
        ]);

        Session::put('MESSAGE', 'ITEM_UPDATED');

        return redirect('/admin/settings')  ;
    }

    public function setting(Request $request)
    {
        $saved = (Session::get('SAVED'));
        if ((int)$saved) {
            Session::forget('SAVED');
        }
        $params = $request->all();

        $terms = [];
        $tmp_terms = DB::select("select * from termpolicy");
        foreach($tmp_terms as $term)
        {
            $tmp = new TermPolicy();
            $tmp = $term;
            $terms[] = $tmp->term_name;
        }

        if (!isset($params['s'])
            // && !in_array($params['s'], ['about_us', 'policy_client', 'policy_shipment', 'policy_refund', 'policy_payment', 'policy_security'])
            && !in_array($params['s'], $terms)
        ) {
            return redirect('/invalid');
        }



        $pageTitle = "Tùy Chỉnh Thông Tin";
        switch ($params['s']) {
            case 'about_us':
                // $pageTitle = "Tùy Chỉnh Giới Thiệu";
                $pageTitle = $this->getTermTitle('about_us',$tmp_terms);
                if (!$this->_viewer->isAllowed('setting_about')) {
                    return redirect('/private');
                }
                break;

            case 'policy_client':
                // $pageTitle = "Tùy Chỉnh Chính Sách Thành Viên";
                $pageTitle = $this->getTermTitle('policy_client',$tmp_terms);
                if (!$this->_viewer->isAllowed('setting_policy')) {
                    return redirect('/private');
                }
                break;

            case 'policy_shipment':
                // $pageTitle = "Tùy Chỉnh Chính Sách Giao Hàng";
                $pageTitle = $this->getTermTitle('policy_shipment',$tmp_terms);
                if (!$this->_viewer->isAllowed('setting_policy')) {
                    return redirect('/private');
                }
                break;

            case 'policy_refund':
                // $pageTitle = "Tùy Chỉnh Chính Sách Đổi Trả";
                $pageTitle = $this->getTermTitle('policy_refund',$tmp_terms);
                if (!$this->_viewer->isAllowed('setting_policy')) {
                    return redirect('/private');
                }
                break;

            case 'policy_payment':
                // $pageTitle = "Tùy Chỉnh Chính Sách Thanh Toán";
                $pageTitle = $this->getTermTitle('policy_payment',$tmp_terms);
                if (!$this->_viewer->isAllowed('setting_policy')) {
                    return redirect('/private');
                }
                break;

            case 'policy_security':
                // $pageTitle = "Tùy Chỉnh Chính Sách Bảo Mật";
                $pageTitle = $this->getTermTitle('policy_security',$tmp_terms);
                if (!$this->_viewer->isAllowed('setting_policy')) {
                    return redirect('/private');
                }
                break;
        }

        $values = [
            'page_title' => $pageTitle,

            'saved' => $saved,

            'key' => $params['s'],
            'terms'=> $tmp_terms
        ];

        //message
        $message = (Session::get('MESSAGE'));
        if (!empty($message)) {
            Session::forget('MESSAGE');
        }
        $values['message'] = $message;

        return view("pages.back_end.settings.tinymce", $values);
    }

    public function getTermTitle($termName,$allTerm){
        foreach($allTerm as $term)
        {
            $tmp = new TermPolicy();
            $tmp = $term;
            if(strcmp($tmp->term_name,$termName) == 0){
                return $tmp->term_title;
            }
        }
    }

    public function allTerms(){
        $terms = DB::select("select * from termpolicy");
       
        return view("pages.be.term.term", ['terms'=>$terms]);
    }

    public function termUpdate(Request $request){
        $term_name = $request->term_name;
        $term_title = $request->term_title;
        $term_id = $request->term_id;

        DB::select("update termpolicy set term_name = '".$term_name."',term_title = '".$term_title."' where term_id = ".$term_id);

        $terms = DB::select("select * from termpolicy");
       
        return view("pages.be.term.term", ['terms'=>$terms]);
        // return redirect()->back()->compact('terms');
    }
    public function termAdd(Request $request){
        $term_name = $request->term_name;
        $term_title = $request->term_title;

        
        DB::insert("Insert into termpolicy(term_name, term_title) values('".$term_name."','".$term_title."')");
        $terms = DB::select("select * from termpolicy");

        return view("pages.be.term.term", ['terms'=>$terms]);

    }

    public function termDeactivate(Request $request){
        $term_id = $request->term_id;
        $term_status = $request->term_status;

        DB::select("update termpolicy set term_status = ".$term_status." where term_id = ".$term_id);
        $terms = DB::select("select * from termpolicy");
       
        return view("pages.be.term.term", ['terms'=>$terms]);
    }

    public function settingUpdate(Request $request)
    {
        $values = $request->post();
//        echo '<pre>';var_dump($values);die;
        unset($values['_token']);
        if (!isset($values['title']) || !isset($values['body'])) {
            return redirect('/invalid');
        }

        $this->_apiCore->updateSetting($values['title'], $values['body']);

        $this->_apiCore->addLog([
            'user_id' => $this->_viewer->id,
            'action' => 'setting_update',
            'item_id' => 0,
            'item_type' => 'setting',
            'params' => json_encode([
                'type' => 'config_' . $values['title'],
            ])
        ]);

        Session::put('MESSAGE', 'ITEM_UPDATED');

        return redirect('/admin/setting?s=' . $values['title']);
    }


}
