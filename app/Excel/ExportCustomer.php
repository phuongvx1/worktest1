<?php

namespace App\Excel;

use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use App\User;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportCustomer implements FromView, WithEvents
{
    private $customer;

    public function setCart($customer)
    {
        $this->customer = $customer;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class=> function(AfterSheet $event) {
                $sheet = $event->sheet;
            },
        ];
    }

    public function view(): View
    {
        return view('excel.export_customer', [
            'customer' => $this->customer,
        ]);
    }
}
