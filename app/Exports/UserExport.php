<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Model\UserCart;

class UserExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data =  DB::select("Select u.name,u.phone,u.email,uc.paid_date,uc.total_cart
        from users u join user_carts uc on (u.id = uc.user_id) 
        where CONVERT(uc.paid_date, int) in (Select min(CONVERT(paid_date,int)) from user_carts 
        where status = 'da_thanh_toan' and deleted = 0 group by user_id)");
        return collect($data);
        
    }
    public function headings(): array{
        return ["NAME", "PHONE","EMAIL","FIRST PAID DATE","TOTAL"];
    }
    public function view(): View
    {
        return view('excel.export_customer', [
            'customer' => $this->customer,
        ]);
    }
}
