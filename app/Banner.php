<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public $table = 'banner';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'title', 'url', 'banner','mobi_banner','status'
    ];

}
