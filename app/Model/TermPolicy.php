<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use App\Api\Core;
use App\Api\FE;
use App\User;
use \Session;
use \Artisan;

class TermPolicy extends Item
{
    public $table = 'termpolicy';
    protected $primaryKey = 'term_id';
    protected $fillable = [
        'term_name', 'term_info', 'term_title','term_status',
    ];

}