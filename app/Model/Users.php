<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use App\Api\Core;
use App\Api\FE;
use App\User;
use \Session;
use \Artisan;

class Users extends Item
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'name', 
        'phone', 
        'address',
        'ward_id',
        'district_id',
        'province_id',
        'email',
        'password',
        'href',
        'level_id',
        'supplier_id',
        'time_notification',
        'note',
        'deleted',
        'remember_token'
    ];

}
